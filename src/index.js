export function slugger(...args){
    const trimmed = args.map(str=>str.trim().replace(/\s+/g, '-'))
    return trimmed.join('-')
    
}

