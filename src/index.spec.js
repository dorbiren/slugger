import { slugger } from "./index.js";


describe('testing slugger basic functionality', () => {
    /**
     * @it - unit tests can use the 'it' syntax
     */
    it('slugger can slug string with spaces', () => {
        expect(slugger('my name is dor birendorf')).toEqual('my-name-is-dor-birendorf');
    })
    /**
     * @test - unit test can use the 'test' syntax
     */
    test('slugger can slug any number of spacy strings', () => {
        const str1='my name   '
        const str2=' is   dor birendorf'
        expect(slugger(str1,str2)).toEqual('my-name-is-dor-birendorf');
        
    })
})
